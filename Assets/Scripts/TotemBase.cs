using HCF;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TotemBase : MonoBehaviour
{
    public event Action<List<Totem>> OnTotemPlacement;
    public List<Totem> _totems;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Player player = other.GetComponentInParent<Player>();
            if (player.role == Player.Role.Attacker && player.totems.Count > 0)
            {
                foreach (Totem totem in player.totems)
                {
                    totem.transform.parent = this.transform;
                    totem.transform.localPosition = Vector2.up * (_totems.Count * 1.2f);
                    totem.GetComponent<YSort>().enabled = false;
                    totem.GetComponent<SpriteRenderer>().sortingOrder = 0;
                    _totems.Add(totem);
                }
                player.OnTotemDrop();
                if (OnTotemPlacement != null)
                {
                    OnTotemPlacement(_totems);
                }
            }
        }
    }
}

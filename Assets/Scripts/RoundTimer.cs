﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoundTimer : MonoBehaviour {
	[SerializeField]
	private Image _image;

	private void Awake() {
		_image = GetComponent<Image>();
	}

	private void Start() {
		RoundManager.instance.OnRoundProgress += UpdateBar;
	}
	
	private void UpdateBar(float amount) {
		_image.fillAmount = amount;
	}
}

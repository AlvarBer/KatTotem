﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace HCF
{
    [RequireComponent(typeof(SortingGroup))]
    public class YSortGroup : MonoBehaviour
    {
        SortingGroup _sortingGroup;

        private void Awake()
        {
            _sortingGroup = GetComponent<SortingGroup>();
        }

        private void Update()
        {
            _sortingGroup.sortingOrder = -Mathf.RoundToInt(transform.position.y * 50);
        }
    }
}
using UnityEditor;
using System;
using System.Linq;

namespace HCF
{
    static class Build
    {
        public static void BuildIt()
        {
            BuildPlayerOptions options = new BuildPlayerOptions
            {
                locationPathName = "Builds/Web",
                scenes = GetEnabledScenes(),
                options = BuildOptions.StrictMode,
                target = BuildTarget.WebGL,
            };
            Console.WriteLine("Buiding...");
            BuildPipeline.BuildPlayer(options);
            Console.WriteLine("Done building");
        }

        private static string[] GetEnabledScenes()
        {
            return EditorBuildSettings.scenes
                .Where(scene => scene.enabled && !string.IsNullOrEmpty(scene.path))
                .Select(scene => scene.path)
                .ToArray();
        }
    }
}
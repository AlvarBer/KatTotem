﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HCF
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class YSort : MonoBehaviour
    {
        SpriteRenderer _spriteRenderer;

        private void Awake()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        private void Update()
        {
            _spriteRenderer.sortingOrder = -Mathf.RoundToInt(transform.position.y * 50);
        }
    }
}

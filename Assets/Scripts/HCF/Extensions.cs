using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HCF
{
    static class Extensions
    {
        /// <summary>
        /// Swaps two values using references
        /// </summary>
        public static T Swap<T>(this T x, ref T y)
        {
            T temp = y;
            y = x;
            return temp;
        }

        // Random functions
        /// <summary>
        /// Returns a random true or false
        /// </summary>
        public static bool RandomBool(this MonoBehaviour r)
        {
            return UnityEngine.Random.value > 0.5;
        }

        /// <summary>
        /// Returns a point inside a frame defined by four points
        /// <param name="xStart">X dimension start</param>
        /// <param name="xFinish">X dimension finish</param>
        /// <param name="yStart">Y dimension start</param>
        /// <param name="yFinish">Y dimension finish</param>
        /// </summary>
        public static Vector2 RandomInsideFrame(
            this MonoBehaviour m,
            float xStart,
            float xFinish,
            float yStart,
            float yFinish
        )
        {
            float x = UnityEngine.Random.Range(xStart, xFinish);
            float y = UnityEngine.Random.Range(yStart, yFinish);
            Vector2 point = new Vector2(x, y);
            float xSign = Mathf.Pow(-1, UnityEngine.Random.Range(1, 3));
            float ySign = Mathf.Pow(-1, UnityEngine.Random.Range(1, 3));
            Vector2 signs = new Vector2(xSign, ySign);
            return Vector2.Scale(point, signs);
        }

        /// <summary>
        /// Invokes a coroutine after duration
        /// </summary>
        public static void RunAfter(this MonoBehaviour m, Action f, float duration)
        {
            IEnumerator RunAfter_(Action action, float dur)
            {
                yield return new WaitForSeconds(dur);
                action();
            }
            m.StartCoroutine(RunAfter_(f, duration));
        }

        /// <summary>
        /// Returns the required drag to a certain speed with an acceleration
        /// <param name="acceleration">Acceleration (in m/s^2)</param>
        /// <param name="terminalSpeed">Max speed (in m/s)</param>
        /// </summary>
        // https://answers.unity.com/questions/819273/force-to-velocity-scaling.html?childToView=819474#answer-819474
        public static float DragRequiredFromAccel(this Rigidbody2D r, float acceleration, float terminalSpeed)
        {
            return DragRequiredFromImpulse(r, acceleration * Time.fixedDeltaTime, terminalSpeed);
        }

        public static float DragRequiredFromImpulse(this Rigidbody2D r, float speed, float terminalSpeed)
        {
            return speed / ((terminalSpeed + speed) * Time.fixedDeltaTime) / r.mass;
        }

        /// <summary>
        /// Remaps a value from a range to another
        /// </summary>
        public static float Remap(this float value, float from1, float to1, float from2, float to2)
        {
            return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
        }
    }
}
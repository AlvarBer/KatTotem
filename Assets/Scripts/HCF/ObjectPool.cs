﻿using HCF;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HCF
{
    public class ObjectPool<T, U> : Singleton<U>
        where T : MonoBehaviour
        where U : MonoBehaviour
    {
        public T prefab;
        public int size;
        public event Action OnPoolFull;
        // All operations should remain O(1)
        private List<T> _freeObjects;
        private List<T> _usedObjects;

        private void Awake()
        {
            _freeObjects = new List<T>(size);
            _usedObjects = new List<T>(size);

            for (var i = 0; i < size; i++)
            {
                T pooledObject = Instantiate(prefab);
                pooledObject.gameObject.SetActive(false);
                _freeObjects.Add(pooledObject);
            }
        }

        public T Instantiate()
        {
            if (_freeObjects.Count == 0)
            {
                return null;
            }
            T pooledObject = _freeObjects[_freeObjects.Count - 1];
            _freeObjects.RemoveAt(_freeObjects.Count - 1);
            _usedObjects.Add(pooledObject);
            return pooledObject;
        }

        public T Instantiate(Vector3 position, Quaternion rotation)
        {
            T pooledObject = Instantiate();
            pooledObject.transform.position = position;
            pooledObject.transform.rotation = rotation;
            pooledObject.gameObject.SetActive(true);
            return pooledObject;
        }

        public void Destroy(T item)
        {
            if (_freeObjects.Contains(item))
            {
                Debug.Log("Object has already been reclaimed");
                return;
            }
            Debug.Assert(_usedObjects.Contains(item), "Object doesn't belong in the pool");
            Debug.Assert(_freeObjects.Count < _freeObjects.Capacity, "Trying to destroy a pooled object twice");
            // This is O(n), should swap with last first
            _usedObjects.Remove(item);
            item.gameObject.SetActive(false);
            _freeObjects.Add(item);
            if (_freeObjects.Count == _freeObjects.Capacity && OnPoolFull != null)
            {
                OnPoolFull();
            }
        }

        public void Clear()
        {
            int usedObjectsLenght = _usedObjects.Count;
            for (int i = 0; i < _usedObjects.Count; i++)
            {
                _usedObjects[i].gameObject.SetActive(false);
                _freeObjects.Add(_usedObjects[i]);
            }
            _usedObjects.Clear();
        }
    }
}

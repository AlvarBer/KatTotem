﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using HCF;

public class RoundManager : Singleton<RoundManager> {
	public event Action<float> OnRoundProgress;
	public float roundLength = 60f;
	[SerializeField]
	private Text _player1Tooltip;
	[SerializeField]
	private Text _player2Tooltip;

	private int _totemsToWin;
	private float _roundTime;
	private bool _playing = false;

	private void Awake() {
		_totemsToWin = FindObjectsOfType(typeof(Totem)).Length;
		TotemBase totemBase = (TotemBase) FindObjectOfType(typeof(TotemBase));
		totemBase.OnTotemPlacement += PlacedTotem;
	}

	private void Start() {
		this.RunAfter(() => {
			_player1Tooltip.gameObject.SetActive(true);
			_player2Tooltip.gameObject.SetActive(true);
		}, 1f);
		this.RunAfter(() => {
			_player1Tooltip.gameObject.SetActive(false);
			_player2Tooltip.gameObject.SetActive(false);
		}, 5f);
		this.RunAfter(StartRound, 5f);
	}

	private void Update() {
		if (_playing) {
			_roundTime += Time.deltaTime;
			if (OnRoundProgress != null) {
				float amountLeft = 1f - (_roundTime / roundLength);
				if (amountLeft <= 0) {
					EndRound(Player.Role.Defender);
				}
				OnRoundProgress(amountLeft);
			}
		}
	}

	private void StartRound() {
		_roundTime = 0;
		_playing = true;
	}

	private void PlacedTotem(List<Totem> totems) {
		if (totems.Count == _totemsToWin) {
			EndRound(Player.Role.Attacker);
		}
	}

	private void EndRound(Player.Role winner) {
		if (winner == Player.Role.Attacker) {
			PlayerPrefs.SetInt("Winner", 1);
		}
		else {
			PlayerPrefs.SetInt("Winner", 2);
		}
		_playing = false;
		Time.timeScale = 0.5f;
		this.RunAfter(() => {
			Time.timeScale = 1;
			SceneManager.LoadScene("Win");
		}, 1f * Time.timeScale);
	}
}

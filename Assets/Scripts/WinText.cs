﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinText : MonoBehaviour {
	[SerializeField]
	private Text _text;

	private void Awake() {
		int winnerIdx = PlayerPrefs.GetInt("Winner");
		_text.text = $"Player {winnerIdx} wins";
	}
}

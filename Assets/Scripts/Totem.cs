﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HCF;

[DisallowMultipleComponent]
[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(SpriteRenderer))]
public class Totem : MonoBehaviour {
	public float mass = 1f;

	[SerializeField]
	private Collider2D _collider;
	[SerializeField]
	private SpriteRenderer _sprite;
	[SerializeField]
	private Vector2 _dropRange = new Vector2(1f, 1.5f);

	private Rigidbody2D _rgb2d;


	public void Drop(Vector2 direction) {
		this.transform.parent = null;
		int randomSign = this.RandomBool() ? 1 : -1;
		float distanceMultiplier = Random.Range(_dropRange.x, _dropRange.y);
		Vector2 perpendicular = Vector2.Perpendicular(direction.normalized);
		Vector2 oldPosition = transform.position;
		transform.position = oldPosition + (randomSign * perpendicular * distanceMultiplier);
		this.RunAfter(() => {
			_collider.enabled = true;
		}, .25f);
	}
}

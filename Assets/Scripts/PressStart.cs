﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;


public class PressStart : MonoBehaviour {
	[SerializeField]
	private string _mainScene;
	[SerializeField]
	private float _initialDelay = 2f;

	private float _roundStart;

	private void Awake() {
		_roundStart = Time.time;
	}

	private void Update() {
		if (Input.GetAxisRaw("Submit") == 1 && Time.time - _roundStart >= _initialDelay) {
			SceneManager.LoadScene(_mainScene);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;

public class CameraEffects : MonoBehaviour {
	[SerializeField]
	private Material _defaultMat;
	[Header("Transition shader settings")]
	[SerializeField]
	private Material _transitionMat;
	[SerializeField]
	[Range(0, 1)]
	private float _cutoff = 0;
	[Header("Chromatic aberration settings")]
	[SerializeField]
	private Material _chromaticAberrationMat;
	[SerializeField]
	private float _aberrationAmount = 0.1f;
	[Header("Shockwave settings")]
	[SerializeField]
	private Material _shockwaveMat;
	[SerializeField]
	private Vector2 _size = new Vector2(1.6f, 0.9f);
	[SerializeField]
	private float _initialWaveRad = 0;
	[SerializeField]
	private float _finalWaveRad = 2;
	[SerializeField]
	private float _duration = 0.5f;
	private Material _material;

	private void Awake() {
		_material = _defaultMat;
	}

	public void OnHit(Vector2 position) {
		StartCoroutine(OnHitCoroutine(position));
	}

	private IEnumerator OnHitCoroutine(Vector2 position) {
		CameraShaker.Instance.ShakeOnce(.3f, 2.5f, .1f, .1f);
		_material = _chromaticAberrationMat;
		// Freeze frame
		Time.timeScale = 0;
		yield return new WaitForSecondsRealtime(0.01666f);
		Time.timeScale = 1;
		_material = _shockwaveMat;
		_shockwaveMat.SetVector("_Size", _size);
		_shockwaveMat.SetFloat("_Radius", _initialWaveRad);
		_shockwaveMat.SetVector("_Center", position);
		float timeWaving = 0;
		float waveRadius;
		while (timeWaving < _duration) {
			yield return null;
			timeWaving += Time.deltaTime;
			waveRadius = Mathf.Lerp(_initialWaveRad, _finalWaveRad, timeWaving / _duration);
			_shockwaveMat.SetFloat("_Radius", waveRadius);
		}
		_material = _defaultMat;
	}

	private void OnRenderImage(RenderTexture src, RenderTexture dst) {
		_chromaticAberrationMat.SetFloat("_AberrationMagnitude", _aberrationAmount);
		_transitionMat.SetFloat("_Cutoff", _cutoff);
		Graphics.Blit(src, dst, _material);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class DashParticles : MonoBehaviour {
	[SerializeField]
	private Player _player;
	[SerializeField]
	private ParticleSystem _particles;

	private void Awake() {
		_player.OnStateChange += ParticleController;
	}

	private void ParticleController(Player.State state) {
		if (state == Player.State.Dashing && !_particles.isEmitting) {
			_particles.Play();
		}
		else if (state == Player.State.Recovering && _particles.isEmitting) {
			_particles.Stop();
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class DirectionIndicator : MonoBehaviour {
	[SerializeField]
	private Player _player;
	[SerializeField]
	private SpriteRenderer _sprite;

	private void Awake() {
		_player.OnDirectionChange += AlignDirection;
	}

	private void AlignDirection(Vector2 newDirection) {
		// In this case I want to set the rotation of the direction indicator
		// (the circle arrow thingy), so I have a Vector2 and I want to set my transform
		// z rotation to the angle of that vector respect to Vector2.right.
		// We can rotate a Quaternion by an euler angle on a certain axis by using
		// Quaternion.AngleAxis, and to get the angle get use Vector2.SignedAngle
		// that gives us an angle x: -180 >= x >= 180
		if (newDirection != Vector2.zero) {
			if (!_sprite.enabled) {
				_sprite.enabled = true;
			}
			transform.rotation = Quaternion.AngleAxis(
				Vector2.SignedAngle(Vector2.right, newDirection),
				Vector3.forward
			);
		}
		else {
			if (_sprite.enabled) {
				_sprite.enabled = false;
			}
		}
	}
}

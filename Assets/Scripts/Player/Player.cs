﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using HCF;

[DisallowMultipleComponent]
[SelectionBase]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
public class Player : MonoBehaviour {
	public enum Role {Attacker, Defender};
	public Role role;
	public event Action<Vector2> OnDirectionChange;
	public event Action<State> OnStateChange;
	[HideInInspector]
	public List<Totem> totems;

	[Header("Required Components")]
	[SerializeField]
	private Rigidbody2D _rb2d;
	[SerializeField]
	private AudioSource _audioSrc;
	[SerializeField]
	private Animator _animator;

	[Header("Player Variables")]
	[SerializeField]
	private int _playerIndex = 0;
	[SerializeField]
	private float _accel = 90f;
	[SerializeField]
	private float _maxSpeed = 8f;
	[SerializeField]
	private float _dashAccel = 90f;
	[SerializeField]
	private float _recoveryDuration = 0.3f;
	[SerializeField]
	private float _dashDuration = 0.2f;
	[SerializeField]
	private Collider2D _playerCollider;
	[SerializeField]
	private PhysicsMaterial2D _regularPhysics;
	[SerializeField]
	private PhysicsMaterial2D _dashingPhysics;

	private Vector2 _lastDirection = Vector2.right;
	private Vector2 _dashDirection;
	private float _lastStateTransitionTime = 0;
	public enum State {Moving, Dashing, Recovering};
	private State __state = State.Moving;
	private State _state {
		get {return __state;}
		set {
			__state = value;
			_lastStateTransitionTime = Time.time;
			if (OnStateChange != null) {
				OnStateChange(__state);
			}
		}
	}


	private void Awake() {
		_rb2d.drag = _rb2d.DragRequiredFromAccel(_accel, _maxSpeed);
	}

	private void FixedUpdate() {
		float horizontal = Input.GetAxisRaw($"Horizontal{_playerIndex}");
		float vertical = Input.GetAxisRaw($"Vertical{_playerIndex}");
		bool dashPressed = Input.GetAxisRaw($"Boost{_playerIndex}") == 1;
		Vector2 direction = new Vector2(horizontal, vertical).normalized;
		if (OnDirectionChange != null) {
			OnDirectionChange(direction);
		}
		if (direction != Vector2.zero) {
			_lastDirection = direction;
		}
		switch (_state) {
			case State.Moving:
				if (dashPressed && totems.Count == 0) {
					_state = State.Dashing;
					_dashDirection = _lastDirection;
					_playerCollider.sharedMaterial = _dashingPhysics;
					_audioSrc.Play();
				}
				else {
					_rb2d.AddForce(direction * _accel);
				}
				break;
			case State.Dashing:
				if (Time.time - _lastStateTransitionTime >= _dashDuration) {
					_rb2d.drag = _rb2d.DragRequiredFromImpulse(_dashAccel, _maxSpeed);
					_state = State.Recovering;
					_playerCollider.sharedMaterial = _regularPhysics;
				}
				else {
					_rb2d.velocity = _dashDirection * _dashAccel;
				}
				break;
			case State.Recovering:
				if (_rb2d.velocity.magnitude * 1.1 <= _maxSpeed) {
					_rb2d.drag = _rb2d.DragRequiredFromAccel(_accel, _maxSpeed);
				}
				if (Time.time - _lastStateTransitionTime >= _recoveryDuration) {
					if (_rb2d.drag == _rb2d.DragRequiredFromImpulse(_dashAccel, _maxSpeed)) {
						Debug.LogError("Player didn't come back to terminal speed before dash");
					}
					_state = State.Moving;
				}
				else {
					_rb2d.AddForce(direction * _accel);
				}
				break;
		}
	}

	private void OnTriggerEnter2D(Collider2D other) {
		switch (other.tag) {
			case "Totem":
				if (role == Role.Attacker && totems.Count == 0) {
					other.enabled = false;
					other.transform.parent = this.transform;
					other.transform.localPosition = Vector2.up;
					_rb2d.mass++;
					totems.Add(other.GetComponent<Totem>());
				}
				break;
		}
	}

	public void OnTotemDrop() {
		_rb2d.mass -= totems.Aggregate(0f, (totalMass, t1) => totalMass + t1.mass);
		totems.Clear();
	}

	private void OnCollisionEnter2D(Collision2D other) {
		if (other.gameObject.tag == "Player") {
			Player otherPlayer = other.gameObject.GetComponent<Player>();
			if (otherPlayer._state == State.Dashing) {
				OnKnockOut();
			}
		}
	}

	private void OnKnockOut() {
		foreach (Totem totem in totems) {
			totem.Drop(_rb2d.velocity);
		}
		OnTotemDrop();
		_animator.SetBool("OnHit", true);
		this.RunAfter(() => _animator.SetBool("OnHit", false), 0.67f);
		Vector2 viewportPosition = Camera.main.WorldToViewportPoint(transform.position);
		Camera.main.GetComponent<CameraEffects>()?.OnHit(viewportPosition);
	}
}

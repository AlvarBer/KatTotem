﻿Shader "AlvarBer/Line Boil/Sprite" {
    Properties {
        [PerRendererData] _MainTex ("Main Texture", 2D) = "white" {}
        [Header(Displacement Settings)]
        [Normal] _DisplTex ("Displacement Texture", 2D) = "bump" {}
        _DisplAmount ("Displacement Amount", Range(0.001, 0.1)) = 0.01
        _DisplMotion ("Displacement Motion", Vector) = (0.7, 0.6, 0, 0)
        [IntRange] _TimeMul ("Time Multiplier", Range(1,30)) = 3
    }
    SubShader {
        Tags {
            "Queue"="Transparent"
            "RenderType"="Transparent"
            "IgnoreProjector"="True"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }
        Cull Off // Necessary for sprite flipping
        Lighting Off
        ZWrite Off

        Pass {
            Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata {
                float4 position : POSITION;
                float2 uv : TEXCOORD0;
                half4 color : COLOR;
            };
            struct v2f {
                float4 position : SV_POSITION;
                float2 uv : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
                half4 color : COLOR;
            };

            sampler2D _MainTex;
            sampler2D _DisplTex;
            float4 _DisplTex_ST;
            float2 _DisplMotion;
            float _DisplAmount;
            float _TimeMul;

            v2f vert(appdata v) {
                v2f i;
                i.position = UnityObjectToClipPos(v.position);
                _DisplTex_ST.zw += floor(_Time.y * _TimeMul) * _DisplMotion;
                i.uv = v.uv;
                i.uv2 = TRANSFORM_TEX(v.uv, _DisplTex);
                i.color = v.color;
                return i;
            }

            float4 frag(v2f i) : SV_TARGET {
                float2 displ = tex2D(_DisplTex, i.uv2).gb;
                displ = displ * 2 - 1;
                float4 col = tex2D(_MainTex, displ * _DisplAmount + i.uv);
                return col * i.color;
            }
            ENDCG
        }
    }
}

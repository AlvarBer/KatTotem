﻿Shader "AlvarBer/Line Boil/Default" {
	Properties {
		[PerRendererData] _MainTex ("Main Texture", 2D) = "white" {}
		[Header(Displacement Settings)]
		[Normal] _DisplTex ("Displacement Texture", 2D) = "bump" {}
		_DisplAmount ("Displacement Amount", Range(0.001, 0.1)) = 0.01
		_DisplMotion ("Displacement Motion", Vector) = (0.7, 0.6, 0, 0)
		[IntRange] _TimeMultiplier ("Time Multiplier", Range(1,30)) = 10
	}
	SubShader {
		Pass {
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			#include "UnityUI.cginc"

			struct appdata {
				float4 position : POSITION;
				float2 uv : TEXCOORD0;
			};
			struct v2f {
				float4 position : SV_POSITION;
				float2 uv : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
			};

			sampler2D _MainTex;
			sampler2D _DisplTex;
			float4 _DisplTex_ST;
			float2 _DisplMotion;
			fixed4 _Color;
			float _DisplAmount;
			float _TimeMultiplier;

			v2f vert(appdata v) {
				v2f i;
				i.position = UnityObjectToClipPos(v.position);
				i.uv = v.uv;
				_DisplTex_ST.zw += floor(_Time.y * _TimeMultiplier) * _DisplMotion;
				i.uv2 = TRANSFORM_TEX(v.uv, _DisplTex);
				return i;
			}

			float4 frag(v2f i) : SV_TARGET {
				float2 displ = tex2D(_DisplTex, i.uv2).gb;
				displ = displ * 2 - 1;
				float4 col = tex2D(_MainTex, i.uv + displ * _DisplAmount);
				return col;
			}
			ENDCG
		}
	}
}

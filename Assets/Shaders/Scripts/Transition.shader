﻿Shader "Custom/Transition" {
    Properties {
        _MainTex ("Texture", 2D) = "white" {}
        _TransTex ("Transition Texture", 2D) = "white" {}
        _Color ("Color", Color) = (1, 1, 1, 1)
        _Cutoff ("Cutoff Value", Range(0, 1)) = 0
    }
    SubShader {
        Cull Off
        ZWrite Off
        ZTest Always
        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata {
                float4 position : POSITION;
                float2 uv : TEXCOORD0;
            };
            struct v2f {
                float4 position : SV_POSITION;
                float2 uv : TEXCOORD0;
            };

            sampler2D _MainTex;
            sampler2D _TransTex;
            float _Cutoff;
            float4 _Color;

            v2f vert(appdata v) {
                v2f o;
                o.position = UnityObjectToClipPos(v.position);
                o.uv = v.uv;
                return o;
            }
            
            float4 frag(v2f i) : SV_TARGET {
                float4 transit = tex2D(_TransTex , i.uv);

                if (transit.r < _Cutoff) {
                    return _Color;
                }
                
                return tex2D(_MainTex, i.uv);
            }
            ENDCG
        }
    }
}

﻿Shader "Custom/Effects/Shockwave" {
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		_Radius ("Radius", float) = 1
		_Thickness ("Thickness", float) = 0.5
		_Center ("Center", vector) = (0.5, 0.5, 0, 0)
		_Size ("Size/Ratio", vector) = (1.6, .9, 0, 0)
		_Hardness ("Hardness", float) = 1
		_Invert ("Invert", Range(-1, 1)) = 0
		_DisplAmount ("Displacement Amount", float) = 0
	}
	SubShader {
		Cull Off
		ZWrite Off
		ZTest Always

		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata {
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			v2f vert (appdata v) {
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			float2 _Center;
			float2 _Size;
			float _Radius;
			float _Hardness;
			float _Invert;
			float _Thickness;
			float _DisplAmount;

			fixed4 frag (v2f i) : SV_Target {
				fixed4 texCol = tex2D(_MainTex, i.uv);
				float distToCenter = length((i.uv - _Center) * _Size);
				float radiusToInner = _Radius - _Thickness / 2;
				float circleGray = saturate(abs(distToCenter - radiusToInner) / _Thickness);
				float circleAlpha = pow(circleGray, pow(_Hardness, 2));
				// invertedAlpha = (_Invert > 0) ? circleAlpha : (1 - circleAlpha)
				float invertGT = _Invert > 0;
				float invertedAlpha = circleAlpha * invertGT - (1 - circleAlpha) * (invertGT - 1);
				half4 mask = (texCol.rgb, invertedAlpha * abs(_Invert) * texCol.a);
				float2 displaced_uv = i.uv + mask * _DisplAmount;
				half4 distCol = tex2D(_MainTex, displaced_uv);
				return distCol;
			}
			ENDCG
		}
	}
}

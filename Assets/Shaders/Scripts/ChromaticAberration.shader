﻿Shader "AlvarBer/ChromaticAberration" {
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		_AberrationMagnitude ("Aberration Magnitude", Range(0.0, 1)) = 0.0005
	}
	SubShader {
		Cull Off
		ZWrite Off
		ZTest Always

		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata {
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f {
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v) {
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			float _AberrationMagnitude;

			fixed4 frag (v2f i) : SV_Target {
				fixed4 col;
				col.r = tex2D(_MainTex, float2(i.uv.x - _AberrationMagnitude, i.uv.y - _AberrationMagnitude)).r;
				col.g = tex2D(_MainTex, i.uv).g;
				col.b = tex2D(_MainTex, float2(i.uv.x + _AberrationMagnitude, i.uv.y + _AberrationMagnitude)).b;
				col.a = 1;
				return col;
			}
			ENDCG
		}
	}
}

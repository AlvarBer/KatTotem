KatTotem
========

TODO
----
* Figure shader maybe switch to particle?
* Fix simultaneous dashes / Make dash fixed distance all the time / Change bouncy materials for proper physics
* Add clash sound
* Dash cooldown indicator
* Slow down `Time.scaleTime` just before definitive totem
* Add confetti effect
* Fade animations
* Redo cover
* Rubber banding on levels
* Tutorials
* Must have component
* Remove _playing from RoundManager
* Add force per hit (AKA smash meter?)
* Fading velocity for shockwave

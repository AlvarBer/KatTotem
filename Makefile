UNITY := '/mnt/c/Program Files/Unity/Hub/Editor/2018.3.0f2/Editor/Unity.exe'
ZIP := 7z.exe

upload-web: Builds/Web/Web.zip
	butler push $^ alvarber/KatTotem:web

Builds/Web/Web.zip: Builds/Web/Build Builds/Web/index.html
	rm $@
	$(ZIP) a $@ $^

Builds/Web/index.html:
	$(UNITY) \
		-batchmode \
		-nographics \
		-projectPath $(pwd) \
		-executeMethod HCF.Build.BuildIt \
		-quit